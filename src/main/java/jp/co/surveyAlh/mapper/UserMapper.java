package jp.co.surveyAlh.mapper;

import jp.co.surveyAlh.entity.UserEntity;

public interface UserMapper {

	int insert(UserEntity user);
	UserEntity getUser(String loginId, String password);

}
